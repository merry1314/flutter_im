import 'package:flutter/material.dart';
import 'dart:async';

/**
 * 加载页面
 */

class LoadingPage extends StatefulWidget {
  _LoadingState createState() => new _LoadingState();
}

class _LoadingState extends State<LoadingPage> {
  void initState() {
    super.initState();
    //在加载页面停留3秒
    new Future.delayed(Duration(seconds: 3), () {
      print("Flutter即时通讯App界面实现...");
      Navigator.of(context).pushReplacementNamed("app");
    });
  }

  Widget build(BuildContext context) {
    return new Center(
      child: Stack(
        children: <Widget>[
          //加载页面居中背景
          Image.asset(
            "images/loading.jpeg",
            fit: BoxFit.cover,
          )
        ],
      ),
    );
  }
}
