import 'package:flutter/material.dart';
import 'message_data.dart';
import 'message_item.dart';

/**
 * 聊天消息列表
 * 聊天主界面
 */

class MessagePage extends StatefulWidget {
  MessagePageState createState() => new MessagePageState();
}

class MessagePageState extends State<MessagePage> {
  Widget build(BuildContext context) {
    return Scaffold(
      //构造列表
      body: ListView.builder(
        //传入数据长度
        itemCount: messageData.length,
        //构造列表项
        itemBuilder: (BuildContext context, int index) {
          //传入messagedata返回列表项
          return new MessageItem(messageData[index]);
        },
      ),
    );
  }
}
