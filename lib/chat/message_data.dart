import 'package:flutter/material.dart';

/**
 * 聊天消息
 */

//消息类型枚举
enum MessageType { SYSTEM, PUBLIC, CHAT, GROUP }

//聊天数据
class MessageData {
  //头像
  String avatar;
  //主标题
  String title;
  //子标题
  String subTitle;
  //消息时间
  DateTime time;
  //消息类型
  MessageType type;

  MessageData(this.avatar, this.title, this.subTitle, this.time, this.type);
}

List<MessageData> messageData = [
  new MessageData(
    'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=2512175699,963259806&fm=26&gp=0.jpg',
    '小明',
    '简笔画',
    new DateTime.now(),
    MessageType.CHAT,
  ),
  new MessageData(
    'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2354195723,1316028922&fm=26&gp=0.jpg',
    '小红',
    '闲聊',
    new DateTime.now(),
    MessageType.CHAT,
  ),
  new MessageData(
    'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3188068477,1359270764&fm=26&gp=0.jpg',
    '广播',
    '公告消息',
    new DateTime.now(),
    MessageType.PUBLIC,
  ),
  new MessageData(
    'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=4191621542,2253864569&fm=26&gp=0.jpg',
    '系统',
    '系统消息',
    new DateTime.now(),
    MessageType.SYSTEM,
  ),
  new MessageData(
    'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2698854439,1749595402&fm=26&gp=0.jpg',
    '群组消息',
    '群组',
    new DateTime.now(),
    MessageType.GROUP,
  )
];
