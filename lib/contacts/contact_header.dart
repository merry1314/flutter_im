import 'package:flutter/material.dart';
import 'contact_item.dart';

/**
 * 好友列表表头
 */

class ContactHeader extends StatelessWidget {
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        ContactItem(
          titleName: '新加好友',
          imageName: 'images/icon_add_friend.png',
        ),
        ContactItem(
          titleName: '聊天室',
          imageName: 'images/groupchat-icon.png',
        ),
      ],
    );
  }
}
