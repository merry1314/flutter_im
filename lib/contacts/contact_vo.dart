import 'package:flutter/material.dart';

/**
 * 好友列表数据
 * 
 */

class ContactVO {
  //字母排列值
  String seationKey;
  //名称
  String name;
  //头像url
  String avatarUrl;
  //构造函数
  ContactVO({@required this.seationKey, this.name, this.avatarUrl});
}

List<ContactVO> contactData = [
  new ContactVO(
    seationKey: 'A',
    name: 'A张三',
    avatarUrl:
        'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2304121150,26054423&fm=26&gp=0.jpg',
  ),
  new ContactVO(
    seationKey: 'A',
    name: '阿黄',
    avatarUrl:
        'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=359937124,2209572796&fm=26&gp=0.jpg',
  ),
  new ContactVO(
    seationKey: 'B',
    name: '波波',
    avatarUrl:
        'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=3149313309,1314504535&fm=26&gp=0.jpg',
  ),
  new ContactVO(
    seationKey: 'C',
    name: '如是',
    avatarUrl:
        'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=642740023,2302580593&fm=26&gp=0.jpg',
  ),
];
